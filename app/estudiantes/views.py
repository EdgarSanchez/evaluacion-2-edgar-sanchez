from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required


from .forms import FormularioEstudiante
from app.modelo.models import EStudiante

# Create your views here.


def principal(request):

    listaEstudiantes = EStudiante.objects.all().filter(
        estado=True).order_by('apellidos')
    context = {
        'lista': listaEstudiantes,
    }
    return render(request, 'estudiantes/principal_estudiante.html', context)


def crear(request):
    formulario = FormularioEstudiante(request.POST)

    if request.method == 'POST':
        if formulario.is_valid():
            datos = formulario.cleaned_data
            estudiante = EStudiante()
            estudiante.cedula = datos.get('cedula')
            estudiante.numMatricula = datos.get('numMatricula')
            estudiante.nombres = datos.get('nombres')
            estudiante.edad = datos.get('edad')
            estudiante.apellidos = datos.get('apellidos')
            estudiante.genero = datos.get('genero')
            estudiante.carrera = datos.get('carrera')

            estudiante.save()
            return redirect(principal)

    context = {
        'f': formulario,
        'mensaje': 'Bienvenidos',
    }
    return render(request, 'estudiantes/crear_estudiante.html', context)
