from django import forms

from app.modelo.models import EStudiante

class FormularioEstudiante(forms.ModelForm):
    class Meta:
        model = EStudiante
        fields = ["cedula", "numMatricula","nombres", "apellidos","edad", "genero","carrera"]