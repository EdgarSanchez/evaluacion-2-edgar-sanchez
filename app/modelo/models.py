from django.db import models

# Create your models here.


class EStudiante(models.Model):

    listaGenero = (
        ('F', 'Femenino'),
        ('M', 'Masculino'),
    )

    cedula = models.CharField(max_length=10)
    numMatricula = models.CharField(max_length=10, null=True)
    nombres = models.CharField(max_length=60)
    apellidos = models.CharField(max_length=60)
    edad = models.IntegerField(null=True)
    email = models.EmailField(max_length=100, null=True)
    genero = models.CharField(max_length=15, choices=listaGenero, null=True)
    estado = models.BooleanField(default=True)
    carrera = models.CharField(max_length=60, null=True)
    
