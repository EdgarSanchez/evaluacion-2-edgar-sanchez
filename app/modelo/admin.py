from django.contrib import admin
from .models import EStudiante


# Register your models here.
class AdminEstudiante(admin.ModelAdmin):
    list_display = ["cedula", "numMatricula","edad", "nombres", "apellidos"]
    list_editable = ["apellidos", "nombres"]
    list_filter = ["apellidos", "genero"]
    search_fields = ["cedula", "nombres", "apellidos"]

    class Meta:
        model = EStudiante


admin.site.register(EStudiante, AdminEstudiante)
